//
//  ChordSelect.cpp
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 21/01/2016.
//
//

#include "ChordSelect.h"

ChordSelect::ChordSelect()
{
    maj.setButtonText("major");
    min.setButtonText("minor");
    pow5.setButtonText("5");
    dim.setButtonText("dim");
    maj7.setButtonText("maj7");
    min7.setButtonText("min7");
    dom7.setButtonText("7");
    m7b5.setButtonText("m7b5");
    
    addAndMakeVisible(maj);
    maj.addListener (this);
    addAndMakeVisible(min);
    min.addListener (this);
    addAndMakeVisible(pow5);
    pow5.addListener (this);
    addAndMakeVisible(dim);
    dim.addListener (this);
    addAndMakeVisible(maj7);
    maj7.addListener (this);
    addAndMakeVisible(min7);
    min7.addListener (this);
    addAndMakeVisible(dom7);
    dom7.addListener (this);
    addAndMakeVisible(m7b5);
    m7b5.addListener (this);
}
ChordSelect::~ChordSelect()
{
    
}
void ChordSelect::guitarMapper(int root, int index)
{
        #define major {7, 5, 4, 3, 5} //major chord
        #define minor {7, 5, 3, 4, 5} //minor chord
        #define power5 {7, 5, 0, 0, 0} //power chord
        #define diminished {6, 6, 3, 0, 0} // diminished chord
        #define major7 {7, 4, 5, 3, 5} //maj7 chord
        #define minor7 {7, 3, 5, 4, 5} //min7 chord
        #define dominant7 {7, 3, 6, 3, 5} //dominant 7 chord
        #define m7b51 {6, 4, 5, 7, 2}
        #define m7b52 {3, 6, 3, 7, 0} //m7b5 chord
    
        const int guitar[9][5] = {power5, major, minor, diminished, major7, minor7, dominant7, m7b51, m7b52};
    
    int remainder = root % 12;
    int size = 0;
    int tempRoot = root;
    
    //notes from E to G#
    if (remainder >= 4 && remainder <=8)
    {
        size = 6;
    }
    //notes from A to C#
    else if (remainder >= 9 || remainder <= 1)
    {
        if(index == 7)
        {
            index++;
        }
        size = 5;
    }
    //notes D/D# and diminished chords
    else if (remainder == 2 || remainder == 3 || index == 3)
    {
        size = 4;
    }
    //power chords
    else if (index == 0)
    {
        size = 3;
    }
    
    for (int i = 0; i < size; i++)
    {
        guitarNotes.add(tempRoot);
        tempRoot += guitar[index][i];
    }
}
void ChordSelect::resized()
{
    int buttonWidth = (getWidth()-20)/5;
    int buttonHeight = getHeight()/10;
    int xPos = buttonWidth*1.34;
    int yPos = buttonHeight*1.25;
    
    maj.setBounds(0, 0, buttonWidth, buttonHeight);
    min.setBounds(xPos, 0, buttonWidth, buttonHeight);
    pow5.setBounds(2*xPos, 0, buttonWidth, buttonHeight);
    dim.setBounds(3*xPos, 0, buttonWidth, buttonHeight);
    maj7.setBounds(0, yPos, buttonWidth, buttonHeight);
    min7.setBounds(xPos, yPos, buttonWidth, buttonHeight);
    dom7.setBounds(2*xPos, yPos, buttonWidth, buttonHeight);
    m7b5.setBounds(3*xPos, yPos, buttonWidth, buttonHeight);
}
void ChordSelect::buttonClicked (Button* button)
{
    if (button == &pow5)
        chordIndex = 0;
    else if (button == &maj)
        chordIndex = 1;
    else if (button == &min)
        chordIndex = 2;
    else if (button == &dim)
        chordIndex = 3;
    else if (button == &maj7)
        chordIndex = 4;
    else if (button == &min7)
        chordIndex = 5;
    else if (button == &dom7)
        chordIndex = 6;
    else
        chordIndex = 7;
}
