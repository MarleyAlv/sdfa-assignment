//
//  GuitarButton.cpp
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 14/01/2016.
//
//

#include "GuitarButton.h"

GuitarButton::GuitarButton()
{
    colour = Colours::black;
    loadImage("../../Images/Steel String.png");
    index = 0;
}
GuitarButton::~GuitarButton()
{
    
}
void GuitarButton::loadImage (StringRef path)
{
    File f = File(File::getCurrentWorkingDirectory().getChildFile (path));
    img = ImageFileFormat::loadFrom(f);
}
void GuitarButton::setIndex (int i)
{
    index = i;
}
void GuitarButton::paint (Graphics& g)
{
    g.setColour(colour);
    g.fillRect(0, 0, getWidth(), getHeight());
    
    g.drawImageWithin(img, 0, 0, getWidth(), getHeight(), 128);
}
void GuitarButton::mouseEnter (const MouseEvent& event)
{
    colour = Colours::gold;
    repaint();
}
void GuitarButton::mouseExit (const MouseEvent& event)
{
    colour = Colours::black;
    repaint();
}
void GuitarButton::mouseDown (const MouseEvent& event)
{
    colour = Colours::goldenrod;
    repaint();
    
    if (index == 0)
    {
        DBG("E");
    }
    else if (index == 1)
    {
        DBG("A");
    }
    else if (index == 2)
    {
        DBG("D");
    }
    else if (index == 3)
    {
        DBG("G");
    }
    else if (index == 4)
    {
        DBG("B");
    }
    else
    {
        DBG("e");
    }
}
void GuitarButton::mouseUp (const MouseEvent& event)
{
    colour = Colours::gold;
    repaint();
}

