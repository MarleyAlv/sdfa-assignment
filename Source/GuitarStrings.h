//
//  GuitarStrings.h
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 14/01/2016.
//
//

#ifndef GUITARSTRINGS_H_INCLUDED
#define GUITARSTRINGS_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuitarButton.h"
/**Collection of custom buttons.
 
 uses previously made button and arranges them like guitar strings
 
 @see GuitarButton
 */
class GuitarStrings  : public Component
{
public:
    /**@param GuitarStrings()
     `constructor*/
    GuitarStrings();
    /**@param ~GuitarStrings()
     Destructor*/
    ~GuitarStrings();
    /*@param resized()
     resizes components whenever called*/
    void resized() override;

private:
    //==============================================================================
    GuitarButton eString;
    GuitarButton aString;
    GuitarButton dString;
    GuitarButton gString;
    GuitarButton bString;
    GuitarButton highEString;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GuitarStrings)
};



#endif /* GUITARSTRINGSBUTTON_H_INCLUDED */
