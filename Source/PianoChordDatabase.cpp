//
//  PianoChordDatabase.cpp
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 25/01/2016.
//
//

#include "PianoChordDatabase.h"

PianoChordDatabase::PianoChordDatabase()
{
    pianoChord = "";
    addAndMakeVisible(chordLabel);
    
    chordLabel.setText("", dontSendNotification);
    
    chordLabel.setJustificationType (36);
    chordLabel.setFont(Font ("OCR A Std", 50, 1));
}
PianoChordDatabase::~PianoChordDatabase()
{
    
}
void PianoChordDatabase::paint (Graphics& g)
{
    g.setColour(Colours::snow);
    g.fillRect(0, 0, getWidth(), getHeight());
}
void PianoChordDatabase::addMidiData(int itemValue)
{
    midiNotes.addAndSort(itemValue);
}
void PianoChordDatabase::removeMidiData(int itemValue)
{
    midiNotes.remove(itemValue);
}
int PianoChordDatabase::getRootNote() const
{
    int arraySize = midiNotes.size();
    int root = 0;
    int remainder = 0;
    
    if (noteOrder > 0 && noteOrder < arraySize)
    {
        root = midiNotes.get(arraySize-noteOrder);
    }
    else
    {
        root = midiNotes.get(0);
    }
    
    remainder = root % 12;
    if (root < 40 || root > 61)
    {
        if (root < 40 && remainder >= 4)
        {
            root = remainder+36;
        }
        else if(root > 61 && remainder <=1)
        {
            root = remainder+60;
        }
        else
        {
            root = remainder+48;
        }
    }
    return root;
}
int PianoChordDatabase::getChordIndex() const
{
    if (chordPos != 0)
    {
        return (chordPos / 2)-1;
    }
    else
    {
        return 2;
    }
}
void PianoChordDatabase::ChordFinder()
{
    const String chords[] = {"min", "3 ",
        "5", "7 5 7 ",
        "maj", "4 3 5 4 ",
        "min", "3 4 5 3 ",
        "dim", "3 3 6 3 ",
        "maj7", "4 3 4 1 4 3 ",
        "min7", "3 4 3 2 3 4 ",
        "7", "4 3 3 2 4 3 ",
        "m7b5", "3 3 4 2 3 3 "};
    
    if (midiNotes.size() > 1)
    {
        for (int i=1; i < midiNotes.size(); i++)
        {
            int tempinterval = midiNotes.get(i)-midiNotes.get(i-1);
            intervals << tempinterval << " ";
        }
        
        int  exit = 0;
        noteOrder = 0;
        chordPos = 0;
        for (int i=1; i < 18 && exit == 0; i+=2)
        {
            if (chords[i].contains(intervals) == true)
            {
                pianoChord = chords[i-1];
                noteOrder = (chords[i].indexOf(intervals))/2;
                chordPos = i-1;
                exit = 1;
            }
            else
            {
                pianoChord = "";
            }
        }
        DBG ("intervals: " << intervals << " chord: " << pianoChord);
    }
    else
    {
        pianoChord = "";
    }
    intervals.clear();
    chordLabel.getTextValue() = pianoChord;
}
void PianoChordDatabase::resized()
{
    chordLabel.setBounds(0, 0, getWidth(), getHeight());
}