//
//  typeArray.h
//  CommandLineTool
//
//  Created by Marley Alveranga on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef TYPEARRAY_H_INCLUDED
#define TYPEARRAY_H_INCLUDED

#include <iostream>
/**dynamic array.
 
array you can add or remove in size*/
template <class Type>
class typeArray
{
public:
    /**@param typeArray()
     `constructor*/
    typeArray()
    {
        data = nullptr;
        arraySize = 0;
    }
    /**@param ~typeArray()
     `destructor*/
    ~typeArray()
    {
        if (data != nullptr)
        {
            delete[] data;
        }
    }
    /**@param add (Type itemValue)
     `adds element to end of array*/
    void add (Type itemValue)
    {
        Type* tempData = new Type[arraySize+1];
        
        for (int i = 0; i < arraySize; i++)
        {
            tempData[i] = data[i];
        }
        
        tempData[arraySize] = itemValue;
        
        if (tempData != nullptr)
        {
            delete [] data;
        }
        
        data = tempData;
        arraySize++;
    }
    /**@param addAtIndex (Type itemValue, int index)
     `adds element to specific point of array*/
    void addAtIndex (Type itemValue, int index)
    {
        Type* tempData = new Type[arraySize+1];
        
        for (int i = 0; i < arraySize; i++)
        {
            if (i < index)
            {
                tempData[i] = data[i];
            }
            else if (i > index)
            {
                tempData[i+1] = data[i];
            }
        }
        
        tempData[index] = itemValue;
        
        if (tempData != nullptr)
        {
            delete [] data;
        }
        
        data = tempData;
        arraySize++;
    }
    /**@param addAndSort (Type itemValue)
     `adds and reorders elements in array*/
    void addAndSort (Type itemValue)
    {
        Type* tempData = new Type[arraySize+1];
        int index = 0;
        
        for (int i = 0; i < arraySize; i++)
        {
            if (data[i] < itemValue)
            {
                tempData[i] = data[i];
                index++;
            }
            else if (data[i] > itemValue)
            {
                tempData[i+1] = data[i];
            }
        }
        
        tempData[index] = itemValue;
        
        if (tempData != nullptr)
        {
            delete [] data;
        }
        
        data = tempData;
        arraySize++;
    }
    /**@param remove (Type itemValue)
     `removes set element from array*/
    void remove (Type itemValue)
    {
        Type* tempData = new Type[arraySize-1];
        int index = arraySize;
        
        for (int i = 0; i < arraySize; i++)
        {
            if (data[i] == itemValue )
            {
                index = i;
            }
            if (i < index)
            {
                tempData[i] = data[i];
            }
            else if (i > index)
            {
                tempData[i-1] = data[i];
            }
        }
        
        delete [] data;
        data = tempData;
        arraySize--;
    }
    /**@param removeAtIndex(int index)
     `removes element at set array*/
    void removeAtIndex (int index)
    {
        Type* tempData = new Type[arraySize-1];
        
        for (int i = 0; i < arraySize; i++)
        {
            if (i < index)
            {
                tempData[i] = data[i];
            }
            else if (i > index)
            {
                tempData[i-1] = data[i];
            }
        }
        
        delete [] data;
        data = tempData;
        arraySize--;
    }
    /**@return get (int index)
     returns array element at index*/
    Type get (int index) const
    {
        if (index >= 0 && index < arraySize)
        {
            return data[index];
        }
        else
        {
            return 0.f;
        }
    }
    /**@return get (Type itemValue)
     returns array index of element*/
    int getIndex (Type itemValue) const
    {
        int index = 0;
        while (index < arraySize && data[index] != itemValue)
        {
            index++;
        }
        return index;
    }
    /**@return size()
     returns size of array*/
    int size() const
    {
        return arraySize;
    }
private:
    Type* data;
    int arraySize;
};

#endif /* TYPEARRAY_H_INCLUDED */
