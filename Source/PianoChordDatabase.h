//
//  PianoChordDatabase.h
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 25/01/2016.
//
//

#ifndef PIANOCHORDDATABASE_H_INCLUDED
#define PIANOCHORDDATABASE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "typeArray.h"
/** stores and works out all the data from midi input.

 Takes the midi data from maincomponent and stores it in array
 @see mainComponent, typeArray
*/
class PianoChordDatabase   : public Component
{
public:
    //==============================================================================
    /**@param PianoChordDatabase()
     `constructor*/
    PianoChordDatabase();
    /**@param ~PianoChordDatabase()
     `destructor*/
    ~PianoChordDatabase();
    /**@param paint (Graphics& g)
     paints desired shape and fill*/
    void paint (Graphics& g) override;
    /**@param addMidiData (int itemValue)
     adds midiData from mainComponent*/
    void addMidiData(int itemValue);
    /**@param removeMidiData (int itemValue)
     removes midiData from mainComponent*/
    void removeMidiData(int itemValue);
    /**@return getRootNote ()
     returns the root note of midi entered*/
    int getRootNote() const;
    /**@return getChordIndex ()
     returns position of chord if inverted or not*/
    int getChordIndex() const;
    /**@param ChordFinder()
     finds chord being played on keyboard*/
    void ChordFinder();
    /*@param resized()
     resizes components whenever called*/
    void resized() override;
private:
    //==============================================================================
    typeArray<int> midiNotes;
    const String chords[18];
    int noteOrder;
    int chordPos;
    Label chordLabel;
    String intervals;
    String pianoChord;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PianoChordDatabase)
};

#endif /* PIANOCHORDDATABASE_H_INCLUDED */
