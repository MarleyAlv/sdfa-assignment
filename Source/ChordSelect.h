//
//  ChordSelect.h
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 21/01/2016.
//
//

#ifndef CHORDSELECT_H_INCLUDED
#define CHORDSELECT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "typeArray.h"
/**Collection of text buttons for selecting chord.
 
 collection of buttons that change the chord produced if clicked
 
 @see Button::Listener, PianoChordDatabase*/
class ChordSelect   : public Component, public Button::Listener
{
public:
    /**@param ChordSelect()
     `constructor*/
    ChordSelect();
    /**@param ~ChordSelect()
     `Destructor*/
    ~ChordSelect();
    /**@param buttonClicked(Button* button
     `checks if button has been clicked*/
    void buttonClicked (Button* button) override;
    /**@param GuitarMapper(int root, int index)
     generates guitar chords from the root note and the chord which is passed as an index*/
    void guitarMapper(int root, int index);
    /*@param resized()
     resizes components whenever called*/
    void resized() override;
private:
    //==============================================================================
    TextButton maj;
    TextButton min;
    TextButton pow5;
    TextButton dim;
    TextButton maj7;
    TextButton min7;
    TextButton dom7;
    TextButton m7b5;
    typeArray<int> guitarNotes;
    int chordIndex;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChordSelect)
};

#endif /* CHORDSELECT_H_INCLUDED */
