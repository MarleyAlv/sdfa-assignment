//
//  GuitarStringsButton.cpp
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 14/01/2016.
//
//

#include "GuitarStrings.h"

GuitarStrings::GuitarStrings()
{
    addAndMakeVisible(eString);
    addAndMakeVisible(aString);
    aString.setIndex(1);
    addAndMakeVisible(dString);
     dString.setIndex(2);
    addAndMakeVisible(gString);
     gString.setIndex(3);
    addAndMakeVisible(bString);
     bString.setIndex(4);
    addAndMakeVisible(highEString);
     highEString.setIndex(5);
}
GuitarStrings::~GuitarStrings()
{
    
}
void GuitarStrings::resized()
{
    int e6 = getWidth()/6;
    int a5 = getWidth()/7.68;
    int d4 = getWidth()/10.62;
    int g3 = getWidth()/16.26;
    int b2 = getWidth()/21.24;
    int e1 = getWidth()/27.6;
    
    int stringWidth = e6+a5+d4+g3+b2+e1;
    int space = (getWidth()-stringWidth)/5;
    
    eString.setBounds(0, 0, e6, getHeight());
    aString.setBounds(e6+space, 0, a5, getHeight());
    dString.setBounds(e6+a5+(2*space), 0, d4, getHeight());
    gString.setBounds(e6+a5+d4+(3*space), 0, g3, getHeight());
    bString.setBounds(e6+a5+d4+g3+(4*space), 0, b2, getHeight());
    highEString.setBounds(e6+a5+d4+g3+b2+(5*space), 0, e1, getHeight());
}