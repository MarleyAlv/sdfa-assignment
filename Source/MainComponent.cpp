/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    audioDeviceManager.setMidiInputEnabled ("Virtual Axiom\n", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    
    setSize (620, 670);
    
    addAndMakeVisible(chordSelection);
    addAndMakeVisible(guitarStrings);
    addAndMakeVisible(pianoChord);
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}
void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage& message)
{
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    if (message.isNoteOnOrOff())
    {
        if (message.getVelocity() > 0)
        {
            pianoChord.addMidiData(message.getNoteNumber());
        }
        else if (message.getVelocity() == 0)
        {
            pianoChord.removeMidiData(message.getNoteNumber());
        }
    }
    pianoChord.ChordFinder();
}
void MainComponent::resized()
{
    int quarterWidth = getWidth()/4;
    int displayHeight = getHeight()/7.75;
    int displayXPos = getWidth()/2.66;
    int guitarYPos = getHeight()/2.5;
    
    pianoChord.setBounds(displayXPos, 10, quarterWidth, displayHeight);
    chordSelection.setBounds(10, displayHeight+20, getWidth(), getHeight());
    guitarStrings.setBounds(quarterWidth, guitarYPos, getWidth()/2, getHeight()/2);
}