//
//  GuitarButton.h
//  JuceBasicWindow
//
//  Created by Marley Alveranga on 14/01/2016.
//
//

#ifndef GUITARBUTTON_H_INCLUDED
#define GUITARBUTTON_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
/**Custom button to be used for GUI. 
 
 uses paint, image, and mouse events to generate an image button
 
 @see CachedComponentImage, ImageFileFormat, MouseListener
 
 */
class GuitarButton   : public Component, public SettableTooltipClient
{
public:
    GuitarButton();
    ~GuitarButton();
    /**@param loadImage(StringRef path)
     passes relative path of image and loads it into file*/
    void loadImage (StringRef path);
    /**@param paint (Graphics& g)
     paints desired shape and fill*/
    void paint (Graphics& g) override;
    /**@param setIndex (int i)
     sets index of custom buttom as there isn't currently a listener capability*/
    void setIndex (int i);
    /**@param mouseEnter (const MouseEvent& event)
     sets actions when mouse is over component*/
    void mouseEnter (const MouseEvent& event) override;
    /**@param mouseExit (const MouseEvent& event)
     sets actions when mouse is stops hovering over component*/
    void mouseExit (const MouseEvent& event) override;
    /**@param mouseDown (const MouseEvent& event)
     sets at when component is clicked on*/
    void mouseDown (const MouseEvent& event) override;
    /**@param mouseUp (const MouseEvent& event)
     sets action when mouse is released*/
    void mouseUp (const MouseEvent& event) override;
    
private:
    //==============================================================================
    Colour colour;
    Image img;
    int index;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GuitarButton)
};

#endif /* GUITARBUTTON_H_INCLUDED */
