/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuitarStrings.h"
#include "ChordSelect.h"
#include "PianoChordDatabase.h"


//==============================================================================
/**
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public MidiInputCallback
{
public:
    //==============================================================================
    /**@param MainComponent()
     `constructor*/
    MainComponent();
    /**@param ~MainComponent()
     `destructor*/
    ~MainComponent();
    /*@param resized()
     resizes components whenever called*/
    void resized() override;
    /*@param handleIncomingMidiMessage(MidiInput*, const MidiMessage& message)()
     recieves incoming midi data*/
    void handleIncomingMidiMessage(MidiInput*, const MidiMessage& message) override;
private:
    //==============================================================================
    ChordSelect chordSelection;
    GuitarStrings guitarStrings;
    AudioDeviceManager audioDeviceManager;
    typeArray<int> midiNotes;
    PianoChordDatabase pianoChord;
    
JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
        
        
#endif  // MAINCOMPONENT_H_INCLUDED
